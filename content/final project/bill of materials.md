Main Features
Showerloop can be be integrated into existing showers
Water can be diverted to a storage/ grey water tank
Fully 12 V DC off-grid system is possible

Dimensions
23 cm x 45 cm x 150 cm*
35 kg
( + 7 kg packaging )

Filtrate
Nilsilän Quartz Sand
Activated Carbon
Geotextile
Prefilter (PP, PE, 5-10 microns)

Fasteners
Varios screws
3d printed valve hoder
3d printer hose fasters
3d printed filter fastener

Pump
12 V DC self-priming membrane pump
The pump can be placed almost anywhere and
can run dry, pumps up to 3.3 bar.

Piping & Valves
21 mm transparent hose
Brass T's, 90 bends and quick connectors
Plastic quick connectors for maintenance

Filters
10mm CNC milled lids
200mm transparent acrylic tube
3 mm laser cut spacers
M8 stainless steel metal rods

UVIR
240V AC [26W]
[ 7 cm diam. x 55 ]

LED UVIR
12V AC [2W]
[ 7 cm diam. ]

Heater & Thermostat
2-3 kW resistive heater
NPT 10k Temperature probe
and Thermostat
Solid State Optocouple
Brass piping for thermostat

Switch board & power housing
IP67 Waterproof containers for electrical devices
8- channel relay module
Ground Fault Circuit Interruptor
Fuses 10A and 2A
12V DC Motor Controller PWM
230V AC - 12V DC Transformer [150W]

*Smart Shower & Sensors
smart controller to automatically control mode selection, regulate water flow and temperature and monitor overall water and energy usage. Next in development is the addtion of sensors to monitor water quality and filter performance.
