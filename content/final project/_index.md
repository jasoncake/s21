---
title: 'final project - showerloop mini'
---

# showerloop
For many years I've been working on showerloop - a water filtering, purifying and recirculating shower. The idea is to save water and energy by doing the process in real time.

I have already worked on it for years and years, sometimes with friends and mostly learning new skills to make it more complicated but also more reliable. This time I want to make it better and more importantly document the work so that I can share it with the world.

If we do end up in a mad maxx like dystopia maybe we can still have water showers. Anyone play Fallout 1 with the water chip?

![Helsinki Fashion Week](hfw.jpg)

![Shematic](schematic.jpg)

![oldversion](oldversions.png)

![ui](ui.png)
I could also try to make a UI for the whole thing. I think this will be a future project but something that I've always been aiming for.

# making a small portable recirculating shower
Also the vanlife and tinyhouse sectors really need tech like this and so I'd like to focus on a system that uses **5 l of water** if possible to _reduce the size and weight of the system_.

# objectives and specs
* good documentation
* simplify design
* 5 litres of water
* less than 1kWh per 10 minute showers
* sensors

Would be nice:

* make it rt
* gas thermostat /
  * alt heat sources to electricity
* lights
  * show temperature and some cool effects


# sketch
![ttt](sensors.png)

# Water sensors
https://www.dfrobot.com/category-68.html


> add img there
