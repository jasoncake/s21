---
title: 'tool wall - building a tool shelf for Space 21'
#bookHidden: 'true'
---

Tool wall
|:--:|
Build a tool wall for Space 21

Use recycled materials



I went in on saturday to build a tool wall for the little workshop in Space 21. 



<img src="assets/screw.png" alt="Screenshot 2022-03-20 at 19.05.22" style="zoom:25%;" />



Alireza came by to setup a 3d print but stayed to help me.





Material: 

* Concrete and masonry screws (leftovers from the copper wall)
* Wood planks (from Cooler Planet 2020)
* Plywood (from Cooler Planet 2020)





|[go to week 02](../week02/index.html)*|
|:--:|
