---
title: 'week 01 - introduction through html'
#bookHidden: 'true'
---

Assignment 01: Introduction
|:--:|
Create a GitLab account and use its CI tools to publish a website. Submit the following.
 Link to your repository, e.g. https://gitlab.com/your-username/digital-fabrication/
Link to the published website, e.g. https://your-username.gitlab.io/digital-fabrication/
Add a picture and description about yourself. At least one paragraph of text.

I made the site following instructions from w3schools.com.
I did some minor CSS but I didn't go very deap into it.

[simple HTML page](https://jasoncake.gitlab.io/fabacademy2022/index.html)
|:--:|

I also copied some interesting css code from codepen to make the background change colors.

[the new about page is here](https://jasoncake.gitlab.io/s21/about/)
|:--:|

![jason](jason2.jpg)

[go to week 02](../week02/index.html)
|:--:|
