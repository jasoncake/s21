
---
title: 'IV_CAD - parametric'
---



Software: `Fusion360`  
Difficulty: `Easy-medium`  
Tools: `sketch, pattern, join, split bodies, make -> component, copy move, 3d pattern`

<video width=100% controls autoplay >
<source src="modlamp.webm" type="video/webm">
Your browser does not support the video tag.  
</video>  


![parameters](assets/parameters.png)
I made a bunch of parameters with the `change parameters`tab in Fusion. I regularly play with the parameters to make sure that they are working correctly. If the model doesn't work then some step was done incorrectly.

![sketching](assets/giphy.gif)
Make a sketch with 2 polygons. They should have different sizes, also they can have different amounts of edges. Intersect one part with the slot for joinery. This part is labelled `tt` I use that as a short command for thickness. This thickness can later be altered simply by changing the user parameters, or going back to the sketch and manually adjusting the width of the rectangle.

![first gif, fusion](assets/giphy-2.gif)CAD

These gifs were made with Giphy.com It's a site that helps you make and store gifs which can also be published on social media apps like `telegram` and `whatsapp`.  

![first gif, fusion](assets/giphy-3.gif)
> tool tip: use the Visual style command to help see what you need / want to during your design process

![](assets/giphy-4.gif)
By going back through the history bar the cutting rectangle can be moved, altering the overall shape relatively quickly. I don't really know how to parametrically change everything without making the model too complicated or confusing. At the moment it's enough to just move things manually.

![](assets/giphy-5.gif)

It has it's own dedicated space.
Below is the same gif but linked to Giphy.com. By clicking on the bottom left hand corner you can see the other gifs that I'm hosting there.  

> <iframe src="https://giphy.com/embed/Xg7ZcbtbvksH6d1Zee" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/aalto-archives-mushroom-guy-Xg7ZcbtbvksH6d1Zee">a statue made by a student in 1974 (Archive number: 1974:18.1A)</a></p>

[go to week 05: VINYL](.../week05/index.html)
|:--:|


[go to week 06](.../week06)
|:--:|
