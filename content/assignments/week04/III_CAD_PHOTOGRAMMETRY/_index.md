---
title: 'III_CAD - photogrammetry'
---

I like to use Fusion360.  
![](assets/fusion.png)  

Fusion is great if you get it for free and technically if you're a CNC shop or sign maker it's at a fair price, but for hobbyists it's no longer a viable software. Since I can get the educator license I'm all good for the time being, though I hope something similar shows up some day, but I doubt anything will be as versatile.

![](assets/polycam.png)
For this test I wanted to see about importing models from Polycam, it's a cheap photogrammetry/LIDAR scanner app for iOS.  

I don't have LIDAR on my phone but I find that the photogrammetry works way better anyway, so I just `snapped a hundred or so pictures of this TV stand`. Sure I could just measure it but I'm really bad with precision, so if I can use a computer or machine to do it for me I'm game.
Polycam is super easy to use so I won't detail it here. The UI is pretty self explanatory and there is a tutorial in the app. After taking the photos it `automagically uploads everything somewhere on the cloud` where I think google servers do the photogrammetry. It's very fast and the time consuming part is just uploading the images.
You get back a model you can look at, add these measurement lines like I did, and upload the model online, or send it to yourself.

_______

<!-- Import the component -->
<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

<!-- Use it like any other HTML element -->
<model-viewer alt="damaged helmet" src="tvstand2.glb" ar ar-modes="webxr scene-viewer quick-look" seamless-poster shadow-intensity="1" camera-controls style="width:70%;height:600px;">
</model-viewer>

https://modelviewer.dev/
`
Made possile with three.js model-viewer working. https://modelviewer.dev/ <- this is the site
`
```
<!-- Import the component -->
<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

<!-- Use it like any other HTML element -->
<model-viewer alt="tv stand polycammodel in glb format" src="tvstand2.glb" ar ar-modes="webxr scene-viewer quick-look" seamless-poster shadow-intensity="1" camera-controls style="width:70%;height:600px;">
</model-viewer>
```
_______

![](assets/02.jpeg)  
<script src="/js/wc.js"></script>
So here's what the tv stand looks like with textures and some dimensions that I added with Polycam.
![](assets/mesh.png)
Here's just the mesh. It's a bit of a mess but the proportions, general shape and angles are all I want.  

Because of the way I rought sketeched it there were lots of strange features which I spent some time cleaning up, but hopefully less than doing everything perfectly. Quick and dirty doesn't always look nice but if it works...

![](assets/01.png)  

test
{{< video label="mp4" mp4="cad04.mp4" >}}


{{< video src="cad03.mp4" controls="yes" autoplay="yes" >}}
sdfsd



so I made a new shortcode that allows for inserting RAW html. Actually I'm confused because I don't think this should work either. But apparently it does.



Workflow for video:
* record video with OBS
  * crop it to the important area
* import the video to adobe rush
  * make edits and add audio
* use handbrake to compress it even further and get the webm format, seems easier.


<video width=100% controls  >
<source src="cad03.webm" type="video/webm">
Your browser does not support the video tag.  
</video>

With handbrake I compressed the file to make it smaller, and webm format

![05](assets/05.png)

[go to PART IV: CAD_PARAMETRIC](../IV_CAD_PARAMETRIC)
|:--:|
