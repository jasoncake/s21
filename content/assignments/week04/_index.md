---
title: 'week 04 - 2D and 3D CAD - vector'
---



# Vector graphics - Adobe Illustrator
I used `Adobe Illustrator CC` - what ever is the newest version at the moment of writing in Feb 2022. It sucked at first because I was so used to how Photoshop works and they are totally different things. Once you learn the program its pretty sweet and since vector graphics is the main thing with Makershops I haven't really gone back to raster except for art.  

I tried to use Inkscape for some time but it's just too ugly for me.  

Here is my masterpiece:
![photorobot diagram](assets/pr.png)
I made this wiring diagram about how the photorobot machine works
I needed to make it for when I moved it in December 2021 - January 2022 from one spot to another on campus. I made a terrible hand drawn sketch as a template and was about to use Microsoft Visio which is ok at making piping and instrumentation diagrams like this ->
![p&i diagram](assets/pr_visio.png)
but you can make nicer stuff with Miro these days.
In any case I did get pretty

![compare inkscape and photoshop](assets/b00eddbc.png) Guess which tool bar is from which program.
{{< details title="Fight" open=false >}}
Inkscape is on the right. Which one do you like more?
{{< /details >}}

 Guess which tool I used to make the pic above?
{{< details title="well?" open=false >}}
Photoshop duh
{{< /details >}}

[go to PART II: RASTER](./II_RASTER/)
|:--:|

[or go to PART III: CAD](./III_CAD/)
|:--:|

[go to week 05](./week05/index.html)
|:--:|
