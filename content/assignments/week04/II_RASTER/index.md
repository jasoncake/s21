---
title: 'II_2D - raster'
---

I just messed around with PS
![](all.png)
[this was a guide for halftone in adobe PS on the youtube](https://www.youtube.com/watch?v=usDDwwraaRk)

but before that I'm compulsive about adding the tools I like and making my own workspace.

![s](16b97c1d.png)  
in the top right corner there's the workspaces.  

After that we can get to work.
> Select > Subject

That picks out just the main object, some sweet algorithm realises what you're looking for, it's especially good finding people.

![](22.png)


Then make it black and white. It should look like this
![](b1cc66ed.png)



Then do
> Filter > Pixelate > Color halftone...

![](71d9d9c6.png)
![](caf900e8.png)

small pixel size and low number makes the dots smaller. You can have the CMYK colors if you want by giving different angles to each channel. Below you can see it without the black and white effect a little, but they are all on the same angle.
![](bd74d500.png)

Back to black and white and here I removed the white parks with
> Select > Color Range  

Using mr. Eyedrop tool I picked out the white spot.

![](88bbbc5e.png)
I also colorized it with Adjust layers and hue or something like that
![](ca067b38.png)


I'm finally in the Matrix
--- upload complete ---

optimization report. Some losses but ok, maybe it's cool
![](optim.png)

[go to PART III: CAD](../III_CAD)
|:--:|
