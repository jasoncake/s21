---
title: 'week 07 - scan & print'
---


<style>
.top {
  position: relative;
  height: 10px;
  background: linear-gradient(141deg, cyan 0%, rebeccapurple 40%, deeppink 90%);
}

</style>
<html>
<div class='top'></div>
</HTML>
Assignment 06: Electronics Production  

Characterize the design rules for the PCB production process at the Aalto Fablab.
|:--:|
Make an in-circuit programmer - create tool paths for the milling machine, mill it and solder components to the board.
Test the board and debug if needed.



<!-- Import the component -->
<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

<!-- Use it like any other HTML element -->
<model-viewer alt="damaged helmet" src="DamagedHelmet-Gamma.glb" ar ar-modes="webxr scene-viewer quick-look" environment-image="comfy_cafe_1k.hdr" seamless-poster shadow-intensity="1" camera-controls style="width:70%;height:600px;">
</model-viewer>

![1](assets/8.jpg)
![1](assets/1.jpg)

![1](assets/3.jpg)
![1](assets/4.jpg)
![1](assets/5.jpg)
![1](assets/6.jpg)
![1](assets/7.jpg)

![1](assets/9.jpg)
![1](assets/10.jpg)
![1](assets/12.jpg)
![1](assets/13.jpg)
![1](assets/14.jpg)

![1](assets/lens-comparison.jpg)
also see my photorobot page for more about photogrammetry
https://studios.aalto.fi/photorobot/lens-test/



<video width=100% controls  >
<source src="banana.mp4" type="video/mp4">
Your browser does not support the video tag.  
</video>

<video width=100% controls  >
<source src="PR1.mp4" type="video/mp4">
Your browser does not support the video tag.  
</video>
