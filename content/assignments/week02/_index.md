---
title: 'week 02 - principles and practices, the final project'
---

Assignment 02: Principles and Practices
|:--:|
Create a sketch and describe an idea of your final project. Do the following:Add a Final Project page on your website
Add a visual sketch of your final project idea to the Final Project page.
Describe your final project. The description should be at least one paragraph long.
Submit a link to the Final Project page on your website.

It has it's own dedicated space here:
[final project page](/s21/final-project/index.html)
|:--:|

[go to week 03](../week03/index.html)
|:--:|
