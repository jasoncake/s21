---
title: 'assignments'

---

# all about assignments


| **task** | description   | complete | difficulty | time   |
|----------|---------------|----------|------------|--------|
| [week 01](/s21/assignments/week01)  | html page     | yes      | ok         | 1 day  |
| [week 02](/s21/assignments/week02)    | final project | yes      | ok         | 1 days |
| [week 03](/s21/assignments/week03)   | git           | yes      | hard       | 4 days |
| [week 04](/s21/assignments/week04/II_RASTER)  | 2D and 3D CAD | yes      | ok         | 1 day  |
| [week 04](/s21/assignments/week04/III_CAD)  | II_RASTER | yes      | ok         | 1 day  |
| [week 04](/s21/assignments/week04)  | III_CAD | yes      | ok         | 2 days  |
| [week 05](/s21/assignments/week05)    | Lasercutting  | nope        | med     | 2 days |
| [week 06](/s21/assignments/week06)   | Electronics 1 |          |            |        |
| [week 07](/s21/assignments/week07)   | Electronics 1 |          |            |        |
| [week 08](/s21/assignments/week08)   | Electronics 1 |          |            |        |
| [week 09](/s21/assignments/week09)   | Electronics 1 |          |            |        |
| [week 10](/s21/assignments/week10)   | Electronics 1 |          |            |        |

[this was made with tables generator](https://www.tablesgenerator.com/markdown_tables)



[go to PART II: CAD](../III_CAD)
|:--:|

[go to PART III: CAD](../III_CAD)
|:--:|
