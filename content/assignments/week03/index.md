---
title: 'week 03 - project management'
---

Assignment 03: Project Management
|:--:|
Create a page for the project management assignment on your website.
Describe how you created your website on that page.
Describe how you use Git and GitLab to manage files in your repository.


_______


Other useful guides and tips can be found on the [_helpme page in the toolbar](/s21/helpme/)

# Download and install the following software:

`Atom`  
![](assets/atom.png)  
add some plug-ins:  
`icon colors`  
`image-paste`
https://atom.io/packages/markdown-image-paste  
![Image paste](assets/markdown-img-paste.png)

# and these:
`Image converter (MacOS)`
this lets you resize images and convert png to jpg
![Image converter](assets/imgconverter.png)   
`Image Optim (MacOS)`
this optimizes compression in png, jpgs and other formats
![Image Optim](assets/imgoptim.png)  
`Install Homebrew` https://brew.sh/
The Missing Package Manager for macOS (or Linux) — Homebrew
`Oh My Zish` https://ohmyz.sh/#install
Make terminal look better and see what's going on with GIT a bit easier.
ohmyzsh cheatsheet:
https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet

`Make gitlab account` https://gitlab.com/jasoncake/j22

`Make a project in Gitlab`
I called it S21 short for Space21 since that's always on my mind these days.

## `Install GIT` with the command
```
brew install git
```


## `Install Hugo`
Follow the guide here
https://gohugo.io/getting-started/quick-start/

## `Get themes` from
https://themes.gohugo.io/
I went with Book theme:
https://themes.gohugo.io/themes/hugo-book/

## `Make ssh key` with this guide
https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
Generating a new SSH key and adding it to the ssh-agent - Git Docs

`add the key to GITLAB` with this guide
https://docs.gitlab.com/ee/ssh/
Use SSH keys to communicate with GitLab
It should look something like this.
![ssh key example](sshkeyexample.png)

Once you've added the key you should get a confirmation e-mail.
![sshkeyexample](assets/sshkeyemail.png)

# Pull the project from GitLab
```
 cd sites
 git clone git@gitlab.com:jasoncake/s21.git
 ```

![gitproject folder view](assets/gitproject.png)

# Configure the congif.toml file
![like this](assets/config1.png)
Note the baseURL = 'link directly to the gitlab website'
I also used [menu.after] to make some links to my own gitlab page, the site hosted on gitlab, a shortcut to http://localhost:1313/s21 and also links to my classmates pages so I can  ~~steal~~ get inspiration from their work.
![like this](assets/config2.png)

Now when trying to deploy the site in the CI/CD pipeline - see How to Git - section below, there was an error. It looked like this:  
![pipeline](assets/failed_pipeline.png)
Here it complaines about the Hugo installation not having the extended version. It can be fixed by doing the changes below:

# Configure the .gitlab-ci.yml file in GITLAB (online)

![like this](assets/gitlab_yml.png)
This part is really important, I had lots of issues apparently becuase the hugo-book theme required hugo extended. The fix was to add the text as seen above on the first row. * I got some help from my friend Avner on this*

![permissions](assets/6e9b078d.png)
At some point there was a complaint that I didn't have permissions to push to the host so I just added it in gitlab.

Now everything should work in the CI/CD pipeline, check it out on Gitlab:
![like this](assets/pipeline.png)

In case stuff is untracked or whatever. Remove everything from the cashe and then add everything in the folder that you’re in and commit it. Then it should upload everything into the project file.

```
git rm -r --cached .  
git add -A  
Git commit -am 'fix'  
```

Remember to navigate to the local repo in terminal


If everything is good (in green) then use:
```
hugo server
```


Will run the site locally

# How to Git
navigate to your local repository page (for me it's like this)
![folder structure](assets/location.png)

```
cd sites/s21  
```

you can already run the hugo server which runs locally by typing

```
hugo server
```

then simply navigate to http://localhost:1313/s21/ and voilá you can see what the site will look like online. Leaving the window open you can see changes near instantaneously and make sure everything is running properly before publishing. (Remember to save for it to update!)

![eg](assets/eg.png)
you can specify individual files but the period (.) adds all the latest changes
```
git add .
```

commit -a adds everything to the commit shelf and -m 'commit message goes here' is needed to update the txt.
```
git commit -a -m 'commit message goes here'
```



Check your status with:
```
git status
```

If everything is good (in green) then use:
```
git push
```

to upload



Then the webpage should get updated, but it might take a while as the pipeline needs to run first. You can see it in the CI/CD pipeline tab in Gitlab.

Use CMD+SHIFT+R to force reload the page!!!!

[go to week 04](../week04/index.html)
|:--:|
