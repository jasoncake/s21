---
title: 'II_LASER'
---

week 05 - 2D Laser Cutting
|:--:|
Characterize your lasercutter's focus, power, speed, rate and kerf.  
Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple ways.  

Software: `Fusion360, Illustrator`  Sites: `thenounproject.com`  
Difficulty: `Easy-medium`  
Tools: `sketch, pattern, join, split bodies, SKETCH MODE: t for trim, `

Laser cutting is super cool. Laser cutters can cut plastic like PP, PE, and Acrylic, engrave glass and apparently if the power is high enough cut glass and ceramics.
Rubber is also doable, as is MDF, HDF, algae, paper, cardboard, fabric, food, lots of stuff.

The main things about lasers:  


       # laser cutters are a fire hazard!  
       # always be watching the laser
       # know where the fire blanket + fire extinguisher is

  * engraving can work with raster images.
  * filetypes:
    * Photoshop
    * Paint
    * Krita
    * `jpg`, `bitmap`, `png`

  * Different power levels go to the laser to make burns deeper.
  * Speed is also kind fo like power (time = energy on material)
  * Frequency - really low frequency will make a dashed line
  * you can engrave and cut
  * you want to engrave before cutting, the piece moves when it detaches from the stock
  * it needs an outline so vector graphics for Cutting
    * text can be outlined
    * some magicians have figured out vector based fonts
  * Software:
    * Illustrator
    * Inkscape (Trace outlines)
    * Powerpoint
    * `dxf`, `pdf`, `ai`, `svg`
  * stock should always be flat
  * excessive smoke is risky, fire even more so
    * burns change the dimensions of press-fit stuff



# HOW THE C02 LASER WORKS

  {{< youtube t4BfQGhhbOQ  >}}
`skip to 1:25 for the technical description`  

![](assets/8f31fb9c.png)
`acrylic atom`  

Fablab gets its lasercutters, vinyl cutter and other machines from http://Lamtekno.fi (Lauttasaari) The plexi or acrylic is from http://Foiltek.fi (West Vantaa). For small orders go to http://Vink.fi (East Vantaa)
common brands include:
OPTIX®
Plexiglas®
ACRYLITE®


For parametric design I documented it in a previous section:
|:--:|
[see IV_CAD_PARAMETRIC](http://localhost:1313/s21/assignments/week04/IV_CAD_PARAMETRIC/)


[go to week 05: VINYL](../week05/index.html)
|:--:|
