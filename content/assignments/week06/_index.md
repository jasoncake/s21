---
title: 'week 06 - Electronics Part I'
---

week 06 - Electronics
|:--:|
Characterize the design rules for the PCB production process at the Aalto Fablab.
Make an in-circuit programmer. Create tool paths for the milling machine, mill it and solder components the board.
Test the board and debug if needed.





## RESOURCES

UPDI here

http://pub.fabcloud.io/programmers/programmer-updi-d11c/

Interactive BOM is pretty cool

http://pub.fabcloud.io/programmers/programmer-updi-d11c/ibom/

CopperCAM tutorial

[Using CopperCAMfor PCB millingwith SRM-20 - YouTube](https://www.youtube.com/watch?v=TvymRjyVZyQ)

SRM-20 Tutorial

[PCB Milling andDrilling with SRM-20 - YouTube](https://www.youtube.com/watch?v=3z4R8HdXYrg)



![updi1](assets/updi1.jpg)

## TOOLS AND PARTS

Here's the equipment used for milling PCBs

| Laptop with CopperCAM Copper board |
| ---------------------------------- |
| PCB Milling Machine                |
| copper board                       |
| 0.2-0.5mm bit                      |
| 1mm bit                            |
| alan key for connecting the collet |
| double sided tape                  |

## STEPS for COPPERCAM

* CopperCAM tutorial

  [Using CopperCAMfor PCB millingwith SRM-20 - YouTube](https://www.youtube.com/watch?v=TvymRjyVZyQ)



## STEPS for ROLAND MDX-20

[SRM-20 Tutorial]([PCB Milling andDrilling with SRM-20 - YouTube](https://www.youtube.com/watch?v=3z4R8HdXYrg))  



* Use double sided tape to fix the board to the bed

* Move the machine into a good position.

* Keep the tool up.

* Attach the milling bit all the way to the top, tighten with alan key

* Lower the Z-axis so that it's almost at the bottom (look at the mechnicsm, it has a hard stop), lift it up a little bit so that the bit has room to mill downwards.

  * Rule of thumb for me is go all the way down then lift 2-3 mm

* loosen the bit with alan key

  *  hold the bit so that it doesn't fall down. (see pic below)



![updi2](assets/updi2.jpg)

* Set XY-axis to 0 and Z- axis to 0.
* press 'CUT'
* Upload the CopperCAM ext/exr file
  * the machine should activate

* Mill the layers in order, changing the milling bits as needed.

  * Don't forget to reset the Z-axis, but leave the X/Y-axis alone

  * Vacuum the parts in between

    <video src="assets/milling1.mp4"></video>

    <video width=100% controls autoplay >
    <source src="milling1.mp4" type="video/mp4">
    Your browser does not support the video tag.  
    </video>  

* clean up and remove the milled board

*  Go to the next step - soldering



## BOM

| n#   | part                                          |
| ---- | --------------------------------------------- |
| 1    | 1uF                                           |
| 2    | 4.99k                                         |
| 1    | Regulator_Linear_LM3480-3.3V-100mA            |
| 1    | D11C14                                        |
| 1    | Conn_PinHeader_2x02_SWD_P2.54mm_Vertical_SMD  |
| 1    | Conn_PinHeader_UPDI_2x03_P2.54mm_Vertical_SMD |
| 1    | ~~Conn_USB_A_Plain~~                          |



![udpi_parts](assets/udpi_parts.gif)



<!--no one can see this right-->

![updi8](assets/updi8.jpg)

## SOLDERING

| Soldering iron                                             |
| ---------------------------------------------------------- |
| Activated Carbon filter + fan                              |
| Flux                                                       |
| Solder                                                     |
| Multimeter                                                 |
| Hot air gun (for electronics - not like a hardware thingy) |
| Solder removal wire                                        |
| Tweezers                                                   |
| Digital Magnifying glass thing.                            |

![updi9](assets/updi9.jpg)
parts before soldering solder

![updi10](assets/updi10.jpg)
capacitor looks pretty terrible

![updi11](assets/updi11.jpg)
the pins are angled

![updi13](assets/updi15.jpg)

overall the solder looks ok on the components. I'm a bit worried that I fried the microcontroller.

![updi12](assets/updi12.jpg)

A bit fat.

![updi14](assets/updi14.jpg)

it's hard to see it but the solder goes almost vertically here for this part.



## PROCESS

direct soldering the component on the copper pads seemed the easiest. I also tried applying solder to the pads before adding the part, but it resulted in non-flat components that needed more fixing.

If I didn't mess around so much it might have only taken 20 minutes, but I kept wanting it to be better and just adding more solder is the wrong way to do it. Remember that french saying: PERFECT IS THE ENEMY OF GOOD

![updi16](assets/updi16.jpg)
