---
title: 'about me'

---

![the thingy](1.png)
# engineering, art, & making fun stuff

I'm an environmental engineer, designer, artist? I advocate for open design but I suck at documentation, so here's me trying to improve myself.

I'm studying Creative Sustainability, CHEM but really I'm just doing anything related to sustainability and making stuff.
I run a new project/studio space in Aalto University called [Space 21](https://www.aalto.fi/en/space-21) and some times teach skills related to digital fabrication.

I used to do all kinds of stuff at Aalto Fablab since 2015 and have experimented with all kinds of making techniques, but I haven't really mastered any in particular. _There's always more to learn._


![photorobot](pr.png)
I also operate a turntable and multicam system called photorobot. I use it to do photogrammetry and digitization of a ceramics archive. It looks like this. More info [*here*](www.studios.aalto.fi/photorobot).

![real pot](realpot.jpg)
The system takes 3 photos from various angles as the object rotates 10 degrees at a time. So we get 3x36 photos per session = 108 photos. Often the photo count is doubled to 216 when the object is rotated onto it's side or upside down to capture the object from every angle.
![reconstructed pot](3dpot.jpg)
Here's a reconstruction made with Reality Capture.

<div class="sketchfab-embed-wrapper"> <iframe title="Teapot 1970:4.1" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="444" height="444" src="https://sketchfab.com/models/9bc67eea55d94a04b8f092385abdfd17/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/teapot-197041-9bc67eea55d94a04b8f092385abdfd17?utm_medium=embed&utm_campaign=share-popup&utm_content=9bc67eea55d94a04b8f092385abdfd17" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Teapot 1970:4.1 </a> by <a href="https://sketchfab.com/aaltoarchive?utm_medium=embed&utm_campaign=share-popup&utm_content=9bc67eea55d94a04b8f092385abdfd17" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Aalto Archive </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=9bc67eea55d94a04b8f092385abdfd17" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

# art

Graphic design, vector graphics, stuff like that.

# engineering

If it's connected to sustainable design - I'm interested.

# making

I love making things with CNC and with traditional methods.
![sandbox](arsandbox.png)


# resources
my gitlab repo
https://jasoncake.gitlab.io/s21/

my HTML page (made before this one)
https://jasoncake.gitlab.io/fabacademy2022/index.html

my class mates pages
https://gitlab.com/aaltofablab/digital-fabrication-2022
