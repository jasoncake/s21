---
title: 'help me page'
toc: 'true'
---
```
# Hello World! 2023!
```

This is a messy page that I use as a little clip board / reference guide for code that I like and such.  

# TOOLS
[Cloud Convert - avi to mp4 online](https://cloudconvert.com)
This is useful for converting fusion360 animations into mp4's which take less storage space.
Note it only does a bit of work for free.  
[Alternatively compress videos with Handbrake](https://handbrake.fr/rotation.php?file=HandBrake-1.5.1.dmg)  

[Make a GIF wiht Giphy](https://giphy.com/create/gifmaker)  


# HUGO THEME
[Hugo-Book theme that I'm using](https://themes.gohugo.io/themes/hugo-book/)  
[hugo shortcodes](https://hugo-book-demo.netlify.app/docs/shortcodes/columns/)

[markdown Reference](https://www.commonmark.org)
[markdown tutorial](https://www.markdowntutorial.com/lesson/6/)  
[markdown tutorial II ](https://www.markdowntutorial.com/lesson/6/)

# markdown
```
 ` this symbol three time on the top and bottom of the text makes a code snippet
```


# GUIDES
Simple GIT GUIDE
https://rogerdudler.github.io/git-guide/

https://www.theodinproject.com/paths/foundations/courses/foundations/lessons/git-basics

https://ohshitgit.com/


# TIPS & TRICKS
![finnish keyboard shortcuts](assets/a6e746f7.png)

# How to GIT for Jason specifically
navigate to your local repository page
```
 cd sites/s21  
```

you can specify individual files but the period (.) adds all the latest changes
```
git add
```

commit -a adds everything to the commit shelf and -m 'commit message goes here' is needed to update the txt.
```
 git commit -a -m 'commit message goes here'
 ```




Check your status with:
```
git status
```
If everything is good (in green) then use:

```
git push
```

to upload


Use CMD+SHIFT+R to force reload the page, but you shouldn't even need it. Depending on the theme you may need to restart the Hugo server if you, for instance, remove a page.

# COMMENTS
https://cusdis.com/

# SHORTCODE
{{< details title="Fight" open=false >}}
Inkscape is on the right. Which one do you like more?
{{< /details >}}

![shortcode](assets/96d425ea.png)

responsive video shortcode
https://roneo.org/en/hugo-create-a-shortcode-for-local-videos/
video with RAWHTML shortcode
https://iamsorush.com/posts/add-video-to-hugo-post/
RAW HTML shortcode
https://anaulin.org/blog/hugo-raw-html-shortcode/


Ctrl + c <- Stop all commands
pwd <- list your location
ls <- list like the old dir command on DOS
cd <- goes back

| **task** | assignment | documentation  | difficulty  
|----------|----------|---|---
| week 1   | yes      |   |cheese|
| week 2   |       no   |   |   
|  week 3     |          |   |  

[this was made with tables generator](https://www.tablesgenerator.com/markdown_tables)

assignments
* week 1
* week 2
* week 3
  * week 4 <- indenting the asterix give you this

https://gohugo.io/content-management/page-bundles/


https://hugo-book-demo.netlify.app/docs/shortcodes/buttons/

When I forget to add the commit message and things get stuck, do the following procedure_

> Esc
> I  	for intert mode
> Esc
> :wq

This lets you unscrew things up.

Also the following are cancel codes.

> Esc
> q
> ctrl+c

|:--:|

´ shift + the button next to backspace for
`code snippets like this`

#codepen
```
https://codepen.io/antoniasymeonidou/pen/abVqBWL


```



How to make a gif with PS

https://blog.hubspot.com/marketing/how-to-create-animated-gif-quick-tip-ht



<!-- Import the component -->
<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

<!-- Use it like any other HTML element -->
<model-viewer alt="damaged helmet" src="DamagedHelmet-Gamma.glb" ar ar-modes="webxr scene-viewer quick-look" environment-image="comfy_cafe_1k.hdr" seamless-poster shadow-intensity="1" camera-controls style="width:70%;height:600px;">
</model-viewer>

´´´
<!-- Import the component -->
<script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

<!-- Use it like any other HTML element -->
<model-viewer alt="damaged helmet" src="DamagedHelmet-Gamma.glb" ar ar-modes="webxr scene-viewer quick-look" environment-image="comfy_cafe_1k.hdr" seamless-poster shadow-intensity="1" camera-controls style="width:70%;height:600px;">
</model-viewer>
´´´
